const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechulerr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

   result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get ("user?" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg" : "Error obteniendo usuarios"
    }

    res.send(response);
    }

  )

}

function getUsersByIdV2 (req, res) {
 console.log("GET /apitechu/v2/users/:id");

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del usuario a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get ("user?" + query + "&" + mLabAPIKey,
 function(err, resMLab, body) {

   if (err) {
     var response = {
       "msg" : "Error obteniendo usuario"
     }
     res.status(500);
   } else {

     if (body.length > 0) {
       var response = body[0];
     } else {
       var response = {
         "msg" : "Usuario no encontrado"
       }
       res.status(404);
     }
   }


   res.send(response);
   }

 )

}

function createUserV1(req, res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser= {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("Usuario añadido");
  }

  function createUserV2(req, res) {
    console.log("POST /apitechu/v2/users");

    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var newUser= {
      "id": req.body.id,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": crypt.hash(req.body.password)
    };

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    httpClient.post ("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario guardado con éxito");
      var response = !err ? body : {
        "msg" : "Error creando usuario"
      }

      res.status(201).send({"msg" : "Usuario guardado con éxito"})

    }
  )
  }

  function deleteUserV1(req, res) {

    console.log("DELETE /apitechu/v1/users/:id");

    console.log("id es " + req.params.id);

    var users = require('../usuarios.json');
    var deleted = false;

    console.log("Usando for normal");
     for (var i = 0; i < users.length; i++) {
     console.log("comparando " + users[i].id + " y " +  req.params.id);
       if (users[i].id == req.params.id) {
         console.log("La posicion " + i + " coincide");
         users.splice(i, 1);
         deleted = true;
         break;
     }
   }
   if (deleted) {
    // io.writeUserDataToFile(users);

    io.writeUserDataToFile(users);
   }

   var msg = deleted ?
   "Usuario borrado" : "Usuario no encontrado."
   console.log(msg);
   res.send({"msg" : msg});
   }

   function createBalanceV1(req, res) {
     console.log("POST /apitechu/v1/balance");

     console.log(req.body.id);
     console.log(req.body.iban);
     console.log(req.body.movimiento);
     console.log(req.body.importe);
     console.log(req.body.balance);

     var newUser= {
       "id": req.body.id,
       "iban": req.body.iban,
       "movimiento": req.body.movimiento,
       "importe": req.body.importe,
       "balance": req.body.balance
     };

     var httpClient = requestJson.createClient(baseMlabURL);
     console.log("Client created");

     httpClient.post ("user?" + mLabAPIKey, newUser,
     function(err, resMLab, body) {
       console.log("Balance guardado con éxito");
       var response = !err ? body : {
         "msg" : "Error creando balance"
       }

       res.status(201).send({"msg" : "Balance guardado con éxito"})

     }
   )
   }

 


module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.createBalanceV1 = createBalanceV1;
