const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechulerr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function loginUserV1(req, res) {
 console.log("POST /apitechu/v1/login");
 console.log(req.body.email);
 console.log(req.body.password);
 var users = require('../usuarios.json');
 var login = false;
 for (user of users) {
 //recorrer la array de usuarios json


   if (user.email == req.body.email && user.password == req.body.password) {

     console.log("Email econtrado, password correcto");
     //encuentra usuario y contraseña y login


     var logged = user.id;
     user.logged = true;
     login = true;
     console.log("Logged in user with id " + user.id);

     break;
   }
 }

    if (login) {

      io.writeUserDataToFile(users)
      //si el login es true, lo inscribe en el listado.
    }

    var msg = logged ?
    "Usuario logeado" : "Usuario o password incorrectos"
    console.log(msg);
    //sí es correcto, mensaje de usuario logeado. Al contrario, incorrecto.

 res.send(msg)


}

 function logoutUserV1(req, res) {

 console.log("POST /apitechu/v1/logout/:id");
 console.log(req.body.userid);
 var users = require('../usuarios.json');
 var logout = false;

 //for (var user = 0; user < users.length; user++){
 for (user of users) {

   if (user.id == req.params.id && user.logged === true) {

     console.log("Usuario encontrado, saliendo de la cuenta");
     //users.splice(user, 1);
     var loggedout = user.id;
     delete user.logged
     logout = true;

     console.log("Logged out de usuario con id " + user.id);

     break;

   }

 }

 if (logout) {

   io.writeUserDataToFile(users)
   //si el logout es true, lo inscribe en el listado.
 }

  var msg = loggedout ?
 "Logout correcto" : "Logout incorrecto";
  console.log(msg);

  res.send(msg)

}


function loginUserV2(req, res) {

 console.log("POST /apitechu/v2/login");

 var query = 'q={"email": "' + req.body.email + '"}';

 console.log("query es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);

 httpClient.get("user?" + query + "&" + mLabAPIKey,

   function(err, resMLab, body) {

      var isPasswordcorrect = crypt.checkPassword(req.body.password, body[0].password);

     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {

       var response = {

         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"

       }

       res.status(401);

       res.send(response);

     } else {

       console.log("Got a user with that email and password, logging in");

       query = 'q={"id" : ' + body[0].id +'}';

       console.log("Query for put is " + query);

       var putBody = '{"$set":{"logged":true}}';

       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),

         function(errPUT, resMLabPUT, bodyPUT) {

           console.log("PUT done");

           var response = {

             "msg" : "Usuario logado con éxito",

             "idUsuario" : body[0].id

           }

           res.send(response);

         }

       )

     }

   }

 );

}

function logoutUserV2(req, res) {

 console.log("POST /apitechu/v2/logout/:id");
 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,

   function(err, resMLab, body) {

     if (body.length == 0) {

       var response = {

         "mensaje" : "Logout incorrecto, usuario no encontrado"

       }

       res.send(response);

     } else {

       console.log("Got a user with that id, logging out");

       query = 'q={"id" : ' + body[0].id +'}';

       console.log("Query for put is " + query);

       var putBody = '{"$unset":{"logged":""}}'

       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),

         function(errPUT, resMLabPUT, bodyPUT) {

           console.log("PUT done");

           var response = {

             "msg" : "Usuario deslogado",

             "idUsuario" : body[0].id

           }

           res.send(response);

         }

       )

     }

   }

 );

}

function getAccountByUserID (req, res) {
 console.log("GET /apitechu/v3/users/:id");

 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del usuario a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get ("user?" + query + "&" + mLabAPIKey,
 function(err, resMLab, body) {

   if (err) {
     var response = {
       "msg" : "Error obteniendo usuario"
     }
     res.status(500);
   } else {

     if (body.length > 0) {
       var response = body[0];
     } else {
       var response = {
         "msg" : "Usuario no encontrado"
       }
       res.status(404);
     }
   }


   res.send(response);
   }

 )

}

function getBalanceByUserID (req, res) {
 console.log("GET /apitechu/balance/users/:id");

 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del usuario a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get ("user?" + query + "&" + mLabAPIKey,
 function(err, resMLab, body) {

   if (err) {
     var response = {
       "msg" : "Error obteniendo usuario"
     }
     res.status(500);
   } else {

     if (body.length > 0) {
       var response = body[0];
     } else {
       var response = {
         "msg" : "Usuario no encontrado"
       }
       res.status(404);
     }
   }


   res.send(response);
   }

 )

}



module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;
module.exports.getAccountByUserID = getAccountByUserID;
module.exports.getBalanceByUserID = getBalanceByUserID;
