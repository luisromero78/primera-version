var eventos = [];

module.exports = (app, rutaeventos) => {

  app.route(rutaeventos)
    .get((req, res) => {

      console.log("GET /apitechu/evento/user");

      var eventosUsuario = getEventosUsuario(req.usuario);
      if (eventosUsuario && eventosUsuario.length > 0)
        res.json(eventosUsuario);
      else
        res.status(204).send();
    })
    .post((req, res) => {

      console.log("POST /apitechu/nuevoevento/user");
      console.log(req.body.nuevoEvento);
      console.log(req.body.usuario);

      var nuevoEvento = req.body
      nuevoEvento._id = eventos.length;
      nuevoEvento.usuario = req.usuario;
      eventos.push(nuevoEvento)
      res.status(201).json(nuevoEvento);
    })
    .delete((req, res) => {
      eventos = [];
      res.status(204).send();
    });
    //creación de eventos y su nexo con id/usuario

  app.route(`${rutaeventos}/:id`)
    .get((req, res) => {

      console.log("GET /apitechu/getevento/user/:id");
      // obtener un evento por id

      var eventosUsuario = getEventoUsuario(req.params.id, req.usuario);
      if (eventosUsuario && eventosUsuario.length > 0)
        res.json(eventosUsuario[0]);
      else
        res.status(404).send();
    })
    .put((req, res) => {
      // actualización un movimiento por id
      var eventosUsuario = getEventoUsuario(req.params.id, req.usuario);
      if (eventosUsuario && eventosUsuario.length > 0) {
        eventosUsuario[0] = req.body;
        res.json(1);
      } else {
        res.status(404).send(0);
      }

    })
    .delete((req, res) => {
      var eventosUsuario = getEventoUsuario(req.params.id, req.usuario);
      if (eventosUsuario && eventosUsuario.length > 0) {
        eventos.splice(req.params.id, 1)
        res.status(204).send(1);
      } else {
        res.status(404).send(0);
      }
    });
// elemntos concretos de un usuario

  var getEventosUsuario = (usuario) => eventos.filter(m => m.usuario == usuario);
  var getEventoUsuario = (id, usuario) => eventos.filter(m => m.usuario == usuario && m._id == id);

  var resError = (err, res) => {
    console.error(err);
    res.status(500).send(err);
  }
}
// pendiente de crear console.log
