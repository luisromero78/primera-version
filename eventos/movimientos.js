
var movimientos = [];

module.exports = (app, rutaMovimientos) => {

  app.route(rutaMovimientos)
    .get((req, res) => {
  //movimientos usuario. Pendiente vincular con users mLabAPIKey
    console.log("GET /apitechu/movimientos/user");

      var movimientosUsuario = getMovimientosUsuario(req.usuario);
      if (movimientosUsuario && movimientosUsuario.length > 0)
        res.json(movimientosUsuario);
      else
        res.sendStatus(204);
    })
    .post((req, res) => {
//crear un movimiento

    console.log("POST /apitechu/nuevomovimiento/user");
    console.log(req.body.nuevoMovimiento);
    console.log(req.body.usuario);

      var nuevoMovimiento = req.body
      nuevoMovimiento._id = movimientos.length;
      nuevoMovimiento.usuario = req.usuario;
      movimientos.push(nuevoMovimiento)
      res.status(201).json(nuevoMovimiento);
    })
    .delete((req, res) => {
      movimientos = [];
      res.sendStatus(204);
    });

  //para obtener todos los movimientos
  app.route(`${rutaMovimientos}/total`)
    .get((req, res) => {

      console.log("GET /apitechu/totalmovimientos/user");

      const total = { ingresos: 0, gastos: 0 };
      var movimientosUsuario = getMovimientosUsuario(req.usuario);
      if (movimientosUsuario && movimientosUsuario.length > 0) {
        movimientosUsuario.forEach(m => {
          if (m.tipo == 1)
            total.ingresos += m.importe ? m.importe : 0;
          else
            total.gastos += m.importe ? m.importe : 0;
        });
        res.json(total);
      }
      else {
        res.json(total);
      }
    });


//para obtener movimientos de una id concreta
  app.route(`${rutaMovimientos}/:id`)
    .get((req, res) => {
      // conseguir un movimiento por id

      console.log("GET /apitechu/movimientos/user/:id");

      var movimientosUsuario = getMovimientoUsuario(req.params.id, req.usuario);
      if (movimientosUsuario && movimientosUsuario.length > 0)
        res.json(movimientosUsuario[0]);
      else
        res.sendStatus(404);
    })
    .put((req, res) => {
      // actualizar un movimiento por id

      console.log("PUT /apitechu/movimientos/user/:id");

      var movimientosUsuarioIndex = getMovimientoUsuarioIndex(req.params.id, req.usuario);
      if (movimientosUsuarioIndex >= 0) {
        movimientos[movimientosUsuarioIndex] = req.body;
        res.json(1);
      } else {
        res.sendStatus(404);
      }

    })
    .delete((req, res) => {

      // borrar un movimiento por id
      console.log("DELETE /apitechu/movimientos/user/:id");

      var movimientosUsuario = getMovimientoUsuario(req.params.id, req.usuario);
      if (movimientosUsuario && movimientosUsuario.length > 0) {
        movimientos.splice(req.params.id, 1)
        res.sendStatus(204);
      } else {
        res.sendStatus(404);
      }
    });


  var getMovimientosUsuario = (usuario) => movimientos.filter(m => m.usuario == usuario);
  var getMovimientoUsuario = (id, usuario) => movimientos.filter(m => m.usuario == usuario && m._id == id);
  var getMovimientoUsuarioIndex = (id, usuario) => movimientos.findIndex(m => m.usuario == usuario && m._id == id);
  var updateMovimientoUsuario = (id, usuario, body) => movimientos.map(m => m.usuario == usuario && m._id == id ? body : m)

  /** Respuesta común a errores */
  var resError = (err, res) => {
    console.error(err);
    res.status(500).send(err);
  }
}
