/**
 * Módulo de lógica intermedia.
 * @module middleware

 * @param {object} app - aplicación de express
 * @param {object} express  framework express
 * @return configura la aplicación
 */
