# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copiar de archivos de carpeta local a apitechu
ADD . /apitechu

# Instalación de dependencias
RUN npm install


# puerto de escucha de la apitechu
EXPOSE 3000

# Comando de inicialización
CMD ["npm", "start"]
