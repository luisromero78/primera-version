require('dotenv').config();

const express = require('express');
const app = express();

const io = require('./io');

const middleware = require('./middleware');

const maestros = require('./eventos/categorias.js');
const movimientos = require('./eventos/movimientos.js');
const eventos = require('./eventos/eventos.js');
const items = require('./eventos/items.js');

const userController = require('./controllers/UserController');

const Authcontroller = require('./controllers/Authcontroller');

var enableCORS = function(req, res, next) {
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
res.set("Access-Control-Allow-Headers", "Content-Type");

next ();

}

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req, res, next) {
  // Para esta ruta de GET
});

app.post('/', function(req, res, next) {
 // Para esta route de POST
});

app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000;

app.listen(port);

console.log("API escuchando en el puerto " + port);


app.get('/apitechu/v1/users', userController.getUsersV1);

app.get('/apitechu/v2/users', userController.getUsersV2);

app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);

app.post('/apitechu/v1/users', userController.createUserV1);

app.post('/apitechu/v2/users', userController.createUserV2);

app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

app.post('/apitechu/v1/login', Authcontroller.loginUserV1);

app.post('/apitechu/v1/logout/:id', Authcontroller.logoutUserV1);

app.post('/apitechu/v2/login', Authcontroller.loginUserV2);

app.post('/apitechu/v2/logout/:id', Authcontroller.logoutUserV2);

app.get('/apitechu/v3/users/:id', Authcontroller.getAccountByUserID);

app.post('/apitechu/v1/balance', userController.createBalanceV1);

app.get('/apitechu/balance/users/:id', Authcontroller.getBalanceByUserID);

app.get ('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello")

    res.send({"msg" :"Hola desde API TechU"});
      }
)

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)

module.exports.useMiddleware = app => {
    const cors = require('cors');
    const bodyParser = require('body-parser')

    app.use(cors());
    //incluido anteriormente, éste para los enventos y otro más para Access-Control-Allow-Origin

    app.use(bodyParser.urlencoded({
        extended: true
    }));
    // como en el body
    app.use(bodyParser.json());

    app.use((req, res, next) =>{
        console.log(`recibida petición: ${req.url}`);
        // Continuación hacia la siguiente función
        next();

    });
}

module.exports = app => {
    maestros(app, './eventos/categorias.js');
    movimientos(app, './eventos/movimientos.js');
    eventos(app, './eventos/eventos.js');
    items(app, './eventos/items.js');
}

     //.id) {
     //     console.log("La posición " + index + " coincide");
     //     users.splice(index, 1);
     //     deleted = true;
     //     break;
     //   }
     //   index++;


     // }    // console.log("Usando Array ForEach");
     // users.forEach(function (user, index) {
     //   console.log("comparando " + user.id + " y " +  req.params.id);
     //   if (user.id == req.params.id) {
     //     console.log("La posicion " + index + " coincide");
     //     users.splice(index, 1);
     //     deleted = true;
     //   }
     // });    // console.log("Usando Array findIndex");
     //     var indexOfElement = users.findIndex(
     //     function(element){
     //     console.log("comparando " + element.id + " y " +   req.params.id);
     //     return element.id == req.params.id
     //   }
     // )
     //


     // console.log("indexOfElement es " + indexOfElement);
     // if (indexOfElement >= 0) {
     //   users.splice(indexOfElement, 1);
     //   deleted = true;
     // }
